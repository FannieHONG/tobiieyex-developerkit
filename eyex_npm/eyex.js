
var S = require('string');
var FixedArray = require("fixed-array");
var NodeCache = require("node-cache");
var HashMap = require('hashmap');
/**
 * Module exports.
 */

module.exports = eyex;

function eyex(udpport,io){
    
    var socket_pool = new HashMap();
    var capacity = 30;
    var array_left = FixedArray(capacity);
    var array_right = FixedArray(capacity);
    var array_real_left = FixedArray(capacity);
    var array_real_right = FixedArray(capacity);
    var socket_cache_pool = new HashMap();
    var real_last_timestamp = "";
    var eye_last_timestamp = "";
    var focus = false;


    io.sockets.on('connection', function(socket) {  
        console.log('Client connected...');
        var remoteAddress = "127.0.0.1";
        socket_pool.set(remoteAddress,socket);
        socket_cache_pool.set(remoteAddress, new NodeCache({ stdTTL: 10, checkperiod: 100 }));
        console.log(socket.id+" connected");

        socket.on('disconnect', function() {
            console.log(socket.id+" disconnected");
        });

    });

    //udp server on udpport
    var dgram = require("dgram");
    var server = dgram.createSocket("udp4");

    server.on("message", function (msg, rinfo) {
        var udp_msg = JSON.parse(msg);
        if ( socket_pool.has(rinfo.address)) {
            var mysocket = socket_pool.get(rinfo.address)
            if ( udp_msg["header"] == "real"){
                if (real_last_timestamp == "" || real_last_timestamp < parseInt(udp_msg["timestamp"])){
                    real_last_timestamp = parseInt(udp_msg["timestamp"])
                    array_real_left.push(parseFloat(udp_msg["x"]));
                    array_real_right.push(parseFloat(udp_msg["y"]));
                    if ( mysocket != 0 ) {
                        mysocket.emit(udp_msg["header"], array_real_left.mean()+";"+array_real_right.mean());
                    }
                }
            }
            else if ( focus && udp_msg["header"] == "focus"){
                var eyexCache = socket_cache_pool.get(rinfo.address)
                eyexCache.get("focus", function(err,value){
                    if (!err) {
                        if (value == undefined){
                            eyexCache.set("focus",0,5);
                            mysocket.emit(udp_msg["header"], udp_msg["x"]+";"+udp_msg["y"]);
                            
                         console.log(udp_msg["header"], udp_msg["x"]+";"+udp_msg["y"]);
                        }
                    }
                })
            }
            else if ( udp_msg["header"] == "attention"){
                if (udp_msg["status"]=="begin"){
                    focus = true;
                }
                if (udp_msg["status"]=="end"){
                    focus = false;
                }
            }
            else{
                var eyexCache = socket_cache_pool.get(rinfo.address)
                if ( udp_msg["header"] == "left" && mysocket != 0){ 
                    if (eye_last_timestamp == "" || eye_last_timestamp < parseInt(udp_msg["timestamp"])){
                        eye_last_timestamp = parseInt(udp_msg["timestamp"])
                        if ( udp_msg["left"] == 1){
                            array_left.push(1);
                            array_right.push(-1);
                            if ( array_right.length() == capacity && array_right.max() == "-1" && array_left.max() != "-1"){
                                eyexCache.get("right", function(err,value){
                                    if (!err) {
                                        if (value == undefined){
                                            eyexCache.set("right",2,10);
                                            console.log("right eye closes");
                                            mysocket.emit('right','click');
                                            array_right = FixedArray(capacity);
                                        }
                                    }
                                })
                            }
                        }
                        else {
                            array_right.push(1);
                            array_left.push(-1);
                            if ( array_left.length() == capacity && array_left.max() == "-1" && array_right.max() != "-1"){
                                eyexCache.get("right", function(err,value){
                                    if (!err) {
                                        if (value == undefined){
                                            eyexCache.set("left",2,10);
                                            console.log("left eye closes");
                                            mysocket.emit('left','click');
                                            array_right = FixedArray(capacity);
                                        }
                                    }
                                })
                            }
                        }
                    }
                }
            }
        }
    });


    server.on("listening", function () {
        var address = server.address();
        console.log("udp server listening " + address.address + ":" + address.port);
    });
    server.bind(udpport);
}