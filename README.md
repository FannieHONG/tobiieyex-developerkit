eyePin.js
======

###Demonstration
(https://fanniehong.bitbucket.io/EyeXDemo.html)
###Installation

Steps to for users to configure the environment:
1. Download ”Tobii Eye Tracking Core Soft- ware v2.10”
2. Install the software
3. Link the Tobii EyeX to your computer
4. Download the Zip file
5. Unzip
6. Execute ”Tobii Data Stream.exe”

Steps to for developers to install npm package:
1. Open terminal
2. Change directory to your project
3. 
``` bash
npm install eyex
```

###How to Use
* require eyex npm in server side
```
	var express = require("express");
	var app = express();
	var http = require(’http’).createServer(app);
	var io = require(’socket.io’)(http);
	var eyex = require(’eyex’);
	
	eyex(<udp port>,io);
```

* Download eyePin.js
* Import JavaScript API on web pages
```
<script src="js/jquery-2.1.1.js">
<script src="/socket.io/socket.io.js"></script>
<script>
	var socket =
	io.connect(<source_address: source_port>;
</script>
<script src="js/eyePin.js"></script>
<script>
	eyePin(socket);
</script>
```
* Download tobii_C#.zip
* Unzip and run "ConsoleApp1.exe"