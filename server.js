//websocket gateway on 8070
var express = require("express");
var app = express();
const path = require('path');
var http = require('http').createServer(app);
var io = require('socket.io')(http);
var eyex = require('eyex');

http.listen(8070);
// init website
console.log("index page: localhost:8070")

eyex(41181,io);

// Routing
app.set('views', __dirname + '/views');
app.use('/js', express.static(path.join(__dirname, 'js')))
app.use('/dict', express.static(path.join(__dirname, 'dict')))
app.use('/css', express.static(path.join(__dirname, 'css')))
app.use('/fonts', express.static(path.join(__dirname, 'fonts')))
app.use('/images', express.static(path.join(__dirname, 'images')))
app.use('/sound', express.static(path.join(__dirname, 'sound')))

app.engine('html', require('ejs').renderFile);

app.set('view engine', 'ejs');

app.get('/', function(req,res,next) {  
    //res.sendFile(__dirname + '/index.html');
    
	res.render('Cookbook.html');
});

app.get('/Cookbook.html', function(req,res,next) {
	res.render('Cookbook.html');
});

app.get('/StoryBook.html', function(req,res) {
    res.render('StoryBook.html');
});

app.get('/Clipboard.html', function(req,res,next) { 
    res.render('Clipboard.html');
});