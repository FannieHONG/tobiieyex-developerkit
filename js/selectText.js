function insertBreakAtPoint(e) {
    var range;
    var textNode;
    var offset;
    var x = document.getElementById("xValue").value;
    var y = document.getElementById("yValue").value;
    
    var range = document.caretRangeFromPoint(x,y);
    var textNode = range.startContainer,
    offset = range.startOffset;
    alert(range);

    if (textNode.nodeType == 3) {
        var startChar = "",endChar = "";
        var limit = 5000;
        do{
            offset --;
            startChar = textNode.nodeValue.substr(offset,1);
            limit --;
        }while(startChar.length === 1 && startChar.match(/[a-zA-Z-']/i) && limit>0);

        startChar = offset;
        limit = 5000;
        do{
            offset ++;
            endChar = textNode.nodeValue.substr(offset,1);
            limit --;
        }while(endChar.length === 1 && endChar.match(/[a-zA-Z-']/i) && limit>0);

        endChar = offset;
        var word = textNode.nodeValue.substr(startChar+1,endChar-startChar-1);
        search(word);
        
    }
}
