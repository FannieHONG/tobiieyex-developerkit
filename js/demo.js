
function search(word) {
    if (word==null)
        var headword = document.getElementById("searchInput").value;
    else
        var headword = word;
    headword = headword.toLowerCase();
    var data = {
        headword: headword
    };

    $.ajax({
        url:'http://api.pearson.com/v2/dictionaries/entries?headword='+headword,
        type: "GET",
        success: function (response) {
            var results = response.results;
            var wordDiv = document.createElement("div");
            wordDiv.className = "aside-block margin-bottom";
            var wordEntry = document.createElement("h3");
            wordEntry.className = "headword";
            wordEntry.innerHTML = headword;
            var translationUL= document.createElement('ul');
            var definitionUL = document.createElement('ul');
            for (i=0;i<results.length;i++){
                var headword_tmp = results[i].headword.toLowerCase();
                if (headword_tmp == headword) {
                    var senses = results[i].senses[0];
                    var translation = senses.translation;
                    var definition = senses.definition;
                    var li_tmp = document.createElement("li");
                    li_tmp.style.color = "black";
                    if (definition != undefined) {
                        li_tmp.innerHTML = definition;
                        definitionUL.appendChild(li_tmp);
                    }
                    if(translation != undefined ){
                        li_tmp.innerHTML = translation;
                        translationUL.appendChild(li_tmp);
                    }
                }
            }
            wordDiv.appendChild(wordEntry);
            var h4 = document.createElement("h4");
            h4.innerHTML = "Definition";
            wordDiv.appendChild(h4);
            wordDiv.appendChild(definitionUL);
            var h4_1 = document.createElement("h4");
            h4_1.innerHTML = "Translation";
            wordDiv.appendChild(h4_1);
            wordDiv.appendChild(translationUL);
            document.getElementById("sideColumn").appendChild(wordDiv);
        },
        error: function (request,status) {
            alert(request);
        }
    });
        
}
    
    function locate(){
        var x = parseFloat(document.getElementById("xValue").value);
        var y = parseFloat(document.getElementById("yValue").value);
        // alert(document.elementFromPoint(x, y).childNodes[1].innerHTML);
        var bodyRect = document.body.getBoundingClientRect();
        postText = document.getElementsByClassName('post-text');
        for (i = 0; i<postText.length;i++){
            var top = postText[i].getBoundingClientRect().top;
            var left = postText[i].getBoundingClientRect().left;
            var bottom = postText[i].getBoundingClientRect().bottom;
            var right = postText[i].getBoundingClientRect().right;
            
            var city = postText[i].childNodes[1].innerHTML;
            // alert('Element '+city+' is at top-'+top+' bottom-'+bottom+'; left-'+left+' right-'+right);
            if (parseFloat(left)<x && x<parseFloat(right) 
                    && parseFloat(top)<y && y<parseFloat(bottom)){
                search(city);
            }
        }

    }