//        var api="AIzaSyDnF_ixxcDy--Z4wYa-ZZh9NsIkw7PzFxmM",
        var api="AIzaSyAaxHKugOqzxtS9EfMIlh_8Uzp6I9zauy8",

//var api = "AIzaSyBK1BiYy0MkTmcxOF55faAVUAekDZ_q8Fs",
cx = "002758417542788730803:eewxiv2wsec";

//var api = "AIzaSyBK1BiYy0MkTmcxOF55faAVUAekDZ_q8Fs",
//cx = "011207673622230610533:26bjd_wdesm";
//cx = "011207673622230610533:4ftnesfc4_e";

var query_JSON = JSON.parse('{ "keyword":"", "page":0}')
var cur_col = 0;
var socket = io.connect("http://127.0.0.1:8070")

function executeQuery(){
    var page = query_JSON.page;
    var query = document.getElementById("searchBox").value;
    if (query == query_JSON.keyword){
        page = page+2;
        query_JSON.page = page;
    }
    else{
        page = 0;
        query_JSON.page = page;
        query_JSON.keyword = query;
        $("#col0").empty();
        $("#col1").empty();
        $("#col2").empty();
        $("#col3").empty();
    }
    for(j=0;j<2;j++){
        jQuery.ajax({
            url: "https://www.googleapis.com/customsearch/v1?key="+api+"&cx="+cx+"&q="+query+"&searchType=image&num=10&start="+(page*10+1),
            type: "GET",

            contentType: 'application/json; charset=utf-8',
            success: function(resultData) {
                var image = resultData.items;
                for (i=0;i<image.length;i++){
                    var IMG_src = image[i].link;
                    var thumb_src = image[i].image.thumbnailLink;
                    var img = document.createElement("img");
//                    img.src = thumb_src;
                    img.src = thumb_src;
                    img.crossOrigin = "Anonymous";

                    var a = document.createElement("a");
                    a.setAttribute("href",IMG_src);
                    a.appendChild(img);
                    
                    updateEyePin(a,'eyeFocus',function(){
                        imageMove(this);
                    });

                    document.getElementById("col"+(cur_col%5)).appendChild(a);
                    cur_col += 1;
                };

            },
            error : function(jqXHR, textStatus, errorThrown) {
                alert("reach search limit");
            },

            timeout: 120000,
        });

    page += parseInt(1);
    }

}

function imageMove(elementNode) {
    
    var duplicate = false;
    $("#clipboard").find("a").each(function(){ 
        if($(this).attr("href")==elementNode.href){
            duplicate =  true;
        } 
    });
    if (!duplicate){
        var clone = elementNode.cloneNode(true);
        clone.download = "";
        getDataUri(elementNode.href, function (base64) {
            $(clone).attr("base64",base64);
        }); 
//        clone.firstElementChild.src = clone.href;
        document.getElementById("clipboard").appendChild(clone);
        updateEyePin(clone,'eyeFocus',function(){
            document.getElementById("clipboard").removeChild(clone);
        });
    }
}

var getDataUri = function (targetUrl, callback) {
    var xhr = new XMLHttpRequest();
    xhr.onload = function () {
        var reader = new FileReader();
        reader.onloadend = function () {
            callback(reader.result);
        };
        reader.readAsDataURL(xhr.response);
    };
    var proxyUrl = 'https://cors-anywhere.herokuapp.com/';
    xhr.open('GET', proxyUrl + targetUrl);
    xhr.responseType = 'blob';
    xhr.send();
};

function printMousePos(event) {
  console.log("clientX: " + event.clientX + " - clientY: " + event.clientY);
}

document.addEventListener("click", printMousePos);

function downloadImg(){
    // Use jszip
    var zip = new JSZip();
    var index = 0;

    $("#clipboard").find("a").each(function(){ 
        try{
            var url = $(this).attr("base64");
            var rx = /^data:(.*);base64/g;
            var data = rx.exec(url);
            var type = data[1].split("/");
            if (type[0] == "image"){
                var base64 =url.replace(/^data:image\/[a-zA-Z.-]{1,};base64,/, "");
                index += 1;
                zip.file(index+"."+type[1],base64, {base64: true});
            }
        }catch(err){console.log(err)}
    });

    if (index!=0){
        zip.generateAsync({type:"blob"})
        .then(function(content) {
            // see FileSaver.js
            saveAs(content, "example.zip");
        })
    }

     $("#clipboard").empty();
}

function rightBlink(){
    $(window).scroll(function() {
        if($(window).scrollTop() + $(window).height() == $(document).height()) {
            executeQuery();
        }
        else{
            $('html,body').animate({scrollTop:y+300}, 'slow');
        }
    });
};

$(function(){
    updateEyePin($("#searchBox"),'gazeIn',function(){
        $("#searchBox").focus();
    });
    
    updateEyePin($("#searchBox"),'gazeOut',function(){
        $("#searchBox").focusout();
    });
});


