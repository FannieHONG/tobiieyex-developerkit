$(function() {

    $.scrollify({
        section:".panel",
        easing:"easeOutQuad",
        scrollSpeed:1100,
        scrollbars:false,
        sectionName:false,
        setHeights:false,
        overflowScroll:false,
        before:function(i) {
            var active = $(".slide.active");

            active.addClass("remove");

            //setTimeout(function() {
            $("[data-slide=" + i + "]").addClass("active");
            active.removeClass("remove active");
            //},300);

        },
        afterRender() {
            $(".panel").each(function() {
                $(this).css("height", parseInt($(window).height())*6 );

                $(this).find(".inner").css("height", $(window).height());
            });
            $.scrollify.update();
            $("[data-slide=0]").addClass("active");
        }
    });
    
    var socket = io.connect("http://127.0.0.1:8070")
    
    function leftBlink(){
        console.log("scroll up");
        $.scrollify.previous();
    }

    function rightBlink(){
        console.log("scroll down");
        $.scrollify.next();
        if ($.scrollify.current().attr("data-section-name") == "analysis"){
            var totalTime = 0;
            $("p").each(function(){
                totalTime += parseInt($(this).attr("countTime"));
            });
            $("#total").innerHTML = millisToMinutesAndSeconds(totalTime);
        }
    }
    
    
//    updateEyePin($(".animal_voice"),'eyeIn',function(){
//        $(this).play
//    });
    
    updateEyePin($(".slide h2"),'eyeFocus',function(){
        $("#animal").attr("src","images/"+$.scrollify.current().attr("data-section-name")+".png");
    });
});

var dict = [];

var lemmatizer = new Lemmatizer();

function search(word) {
    
    if (word.indexOf(word)>-1) {
        word = lemmatizer.only_lemmas(word, "")[0]

        if (word==null)
            var headword = document.getElementById("searchInput").value;
        else
            var headword = word;
        headword = headword.toLowerCase();
        var data = {
            headword: headword
        };

        $.ajax({
            url:'http://api.pearson.com/v2/dictionaries/entries?headword='+headword,
            type: "GET",
            success: function (response) {
                try{
                    var results = response.results;
                    var wordDiv = document.createElement("div");
                    wordDiv.className = "aside-block margin-bottom";
                    var definitionUL = document.createElement('ol');
                    for (i=0;i<results.length;i++){
                        var headword_tmp = results[i].headword.toLowerCase();
                        if (headword_tmp == headword) {
                            var senses = results[i].senses[0];
                            var definition = senses.definition;
                            var li_tmp = document.createElement("li");
                            li_tmp.style.color = "white";

                            if (definition != undefined) {
                                li_tmp.innerHTML = definition;
                                definitionUL.appendChild(li_tmp);
                            }
                        }
                    }
                    var heading = document.createElement("h3");
                    heading.innerHTML = headword;
                    wordDiv.appendChild(heading);
                    wordDiv.appendChild(definitionUL);
                    alertify.set('notifier','position', 'top-right');
                    alertify.success(wordDiv, 10);
                }catch(err){};

            },
            error: function (request,status) {
                alert(request);
            }
        }); 
    }
}

function focusClick(textNode){
    var offset = returnFocusOffset();
        
    if (textNode.parentNode.nodeName=="MARK") {
        var mark = textNode.parentNode;
        var text = document.createTextNode(mark.innerHTML);
        textNode.parentNode.parentNode.replaceChild(text,mark);
        while (dict.indexOf(text.nodeValue,) !== -1) {
          dict.splice(dict.indexOf(text.nodeValue,), 1);
        }
        $("#dict").innerHTML = dict.toString();
        
    }
    else if (textNode.nodeType == 3) {
        var startChar = "",endChar = "";
        var limit = 5000;
        do{
            offset --;
            startChar = textNode.nodeValue.substr(offset,1);
            limit --;
        }while(startChar.length === 1 && startChar.match(/[a-zA-Z-']/i) && limit>0);

        startChar = offset;
        limit = 5000;
        do{
            offset ++;
            endChar = textNode.nodeValue.substr(offset,1);
            limit --;
        }while(endChar.length === 1 && endChar.match(/[a-zA-Z-']/i) && limit>0);

        endChar = offset;

        var replacement = textNode.nodeValue.substr(startChar+1,endChar-startChar-1);
        var mark = document.createElement('mark');
        var t = document.createTextNode(textNode.nodeValue.substr(startChar+1,endChar-startChar-1));
        mark.appendChild(t);

        textNode.parentElement.innerHTML = textNode.parentElement.innerHTML.replace(replacement, "<mark>"+replacement+"</mark>");

        search(textNode.nodeValue.substr(startChar+1,endChar-startChar-1));
        dict.push(textNode.nodeValue.substr(startChar+1,endChar-startChar-1));
        document.getElementById('dict').innerHTML = dict.toString();

    }
}

function millisToMinutesAndSeconds(millis) {
  var minutes = Math.floor(millis / 60000);
  var seconds = ((millis % 60000) / 1000).toFixed(0);
  return minutes + ":" + (seconds < 10 ? '0' : '') + seconds;
}

function printMousePos(event) {
  console.log("clientX: " + event.clientX + " - clientY: " + event.clientY);
}

document.addEventListener("click", printMousePos);