var xFocus= 403 ,yFocus= 358 ,xReal,yReal;
var showMouse = true;
var hoverStyle = "click";
var clickElement = $('[onclick],a[href^="#"]');
var hoverElement = $('*');
var key = 13;
var socket;
var lastRealEle = "";
var secstart = (new Date()).getTime() / 1000;
var positionTop = window.screenY+window.outerHeight-window.innerHeight;
var positionLeft = window.screenX+window.outerWidth-window.innerWidth;


function eyePin(socket1){
    socket = socket;

    if (clickElement == "all"){
        clickElement = $('[onclick],a[href^="#"]');
    }
    else if (typeof clickElement === 'string'){
        clickElement = $(clickElement);
    }
    
    for (var i = 0, len = clickElement.length; i < len; i++) {
        $(clickElement[i]).on("eyeFocus", function(){
            eyeFocus($(this));
        });
    }
    
    if (hoverElement == "all"){
        hoverElement = $('*');
    }
    else if (typeof hoverElement === 'string'){
        hoverElement = $(hoverElement);
    }
    
    for (var i = 0, len = hoverElement.length; i < len; i++) {
        $(hoverElement[i]).on("gazeOut", function(){
            gazeOut($(this));
        });
        $(hoverElement[i]).on("gazeIn", function(){
            gazeIn($(this));
        });
    }
    
};

$(document).on('keydown', function(e){
    var downKey = e.which;
    if(downKey == key)  // the enter key ascii code
    {
        try{
            eventElement(returnFocusTextNode(),"eyeFocus");
            console.log(xFocus,yFocus);
        }catch(err){
            console.log(err);
        }
    }
});




window.onresize = function(event) {
    positionTop = window.screenY+window.outerHeight-window.innerHeight;
    positionLeft = window.screenX+window.outerWidth-window.innerWidth;
}

window.onmousemove = function(event){
    document.getElementById("circle").style.top = window.innerHeight;
    document.getElementById("circle").style.left = window.innerWidth;
}

if (socket){
    socket.on("focus", function(data) {
        var x = data.split(";")[0];
        var y = data.split(";")[1];

        x = x - positionLeft;
        y = y - positionTop;

        if ( x >= 0 && x <= window.innerWidth 
            && y <= window.innerHeight && y >= 0){
            xFocus = x;
            yFocus = y;
            console.log(xFocus,yFocus);

            switch (hoverStyle){
                case "fire":
                    fire(xFocus,yFocus);
                    break;
                case "click":
                    try{
                        fire(xFocus,yFocus);
                        eventElement(returnFocusTextNode(),"click");
                    }catch(err){
                        console.log(err);
                    }
                    break;
                default:
                    eventElement(returnFocusTextNode(),"eyeFocus");
                    break;
            }
        }

    });

    socket.on("real", function(data) {
        var timeDiff = data.split()[1];
        var x = data.split()[0].split(";")[0];
        var y = data.split()[0].split(";")[1];

        x = x - positionLeft;
        y = y - positionTop;

        if ( x >= 0 && x <= window.innerWidth 
            && y <= window.innerHeight && y >= 0){
            xReal = x;
            yReal = y;
            if (showMouse){
                brighter(x,y);
            }
            accumTime(lastRealEle,timeDiff);
        
            $(lastRealEle).trigger("gazeOut");

            lastRealEle = returnRealTextNode();

            $(lastRealEle).trigger("gazeIn");
        }
        
    });

    socket.on("left", function(data) {
        leftBlink();
    });

    socket.on("right", function(data) {
        rightBlink();
    });
}
    
function fire(x,y){

    var width = window.innerWidth;
    var height = window.innerHeight;

    var x = x/width*100;
    var y = y/height*100;

    if (x<34) {
        var startX = x-1
    }
    else if (x>66) {
        var startX = x+1
    }
    else{
        var startX = x
    }

    if (y<34) {
        var startY = y-1
    }
    else if (y>66) {
        var startY = y+1
    }
    else{
        var startY = y
    }

    createFirework(2,25,3,null,startX,startY,x,y,true,true);
    return false;

}

function brighter(x,y){
    var seconds = (new Date()).getTime() / 1000;
    var base = "0";
    var interval = 6;
    var sec= interval * (seconds - secstart);
    var total= (base + sec);
    document.getElementById("circle").style.color = "hsl("+total+", 65%, 25%)";
    document.getElementById("circle").style.top = y + 'px';
    document.getElementById("circle").style.left = x + 'px';
}

function returnRealTextNode(){
    return returnTextNode(xReal,yReal);
}


function returnFocusTextNode(){
    return returnTextNode(xFocus,yFocus);
}

function returnRealOffset(){
    return returnOffset(xReal,yReal);
}


function returnFocusOffset(){
    return returnOffset(xFocus,yFocus);
}

function returnTextNode(x,y){
    var range;
    var textNode;
    var offset;
    
    try {
        document.getElementById("circle").style.zIndex = -1;

        if (document.caretPositionFromPoint) {
            range = document.caretPositionFromPoint(x,y);
            textNode = range.offsetNode;
            offset = range.offset;

        } else if (document.caretRangeFromPoint) {
            range = document.caretRangeFromPoint(x,y);
            textNode = range.startContainer;
            offset = range.startOffset;
        }

        document.getElementById("circle").style.zIndex = 1000;
        return textNode;
    }catch(err){
        return false;
    }
}

function returnOffset(x,y){
    var range;
    var textNode;
    var offset;

    try{
        document.getElementById("circle").style.zIndex = -1;

        if (document.caretPositionFromPoint) {
            range = document.caretPositionFromPoint(x,y);
            textNode = range.offsetNode;
            offset = range.offset;

        } else if (document.caretRangeFromPoint) {
            range = document.caretRangeFromPoint(x,y);
            textNode = range.startContainer;
            offset = range.startOffset;
        }

        document.getElementById("circle").style.zIndex = 1000;
        return offset;
    }catch(err){
        return false;
    }
}


function eventElement(textNode,event){
    if (textNode != false) {
        $(textNode).trigger(event);

        if ($(textNode).find(clickElement).length != 0) {
            clickable = $(textNode).find(clickElement);
            $(clickable).trigger(event);
            return true;
        }
        else if ($(textNode).closest(clickElement).length != 0){
            clickable = $(textNode).closest(clickElement);
            $(clickable).trigger(event);
            return true;
        }
    }
    return false;
}
   
function accumTime(ele,timeDiff){
    if ($(ele).has("countTime")){
        $(ele).attr("countTime",$(ele).attr("countTime")+timeDiff);
    }
    else{
        $(ele).attr("countTime",0);
    }
}

function gazeIn(ele){
    $(ele).css("opacity","1");
    $(ele).css("filter","blur(1px)");
//    $(ele).css("transform","scale(1.1)");
    $(ele).children().each(function(){
        $(this).css("opacity","1");
        $(this).css("filter","blur(1px)");
//        $(this).css("transform","scale(1.1)");
    })
}

function gazeOut(ele){
    $(ele).css("filter","blur(0px)");
    $(ele).css("opacity","0");
//    $(ele).css("transform","scale(1)");
    $(ele).children().each(function(){
        $(this).css("filter","blur(0px)");
        $(this).css("opacity","0");
//        $(this).css("transform","scale(1)");
    })
    $(ele).css("opacity","0");
}

function eyeFocus(ele){
    ele.click();
}

function leftBlink(){};
function rightBlink(){};

function updateEyePin(ele,event,func){
    clickElement = clickElement.add(ele);

    $(ele).each(function(){
        $(this).off(event).on(event,func);
    });
}